# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class saldo_app(models.Model):
#     _name = 'saldo_app.saldo_app'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Movimiento (models.Model):
    _name = "sa.movimiento" #sa_movimiento
    name = fields.Char ("Movimiento")
    monto = fields.Float ("Monto")

    tipo = fields.Selection (string="Tipo de Movimiento",selection = [("i","Ingreso"),("e","Egreso")])

